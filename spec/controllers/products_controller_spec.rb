require 'rails_helper'
require 'spree/testing_support/factories'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe 'GET #show' do
    let(:product) { create(:product) }

    before do
      get :show, params: { id: product.id }
    end

    it "HTTPが正しく返されているか" do
      expect(response).to have_http_status(:success)
    end

    it '出力されたテンプレートがshowテンプレートかどうか' do
      expect(response).to render_template :show
    end

    it '正しくproductを割り当てているか' do
      expect(assigns(:product)).to eq product
    end
  end
end
