require 'rails_helper'
require 'spree/testing_support/factories'

RSpec.describe ApplicationHelper, type: :helper do
  include ApplicationHelper
  describe 'full_title' do
    it "引数が空文字の場合ベースタイトルを返す" do
      expect(full_title("")).to eq("BIGBAG store")
    end

    it "引数がnilの場合ベースタイトルを返す" do
      expect(full_title(nil)).to eq("BIGBAG store")
    end

    it "商品のフルタイトルを返す" do
      expect(full_title('Ruby on Rails Bag')).to eq("Ruby on Rails Bag - BIGBAG store")
    end
  end
end
